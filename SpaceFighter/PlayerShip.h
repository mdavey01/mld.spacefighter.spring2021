
#pragma once

#include "Ship.h"
#include "PlayerShipShield.h"

class PlayerShip : public Ship
{

public:

	PlayerShip();
	virtual ~PlayerShip() { delete m_pShield; }

	virtual void LoadContent(ResourceManager *pResourceManager);

	virtual void Update(const GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch);

	virtual void HandleInput(const InputState *pInput);

	virtual void Initialize(Level *pLevel, Vector2 &startPosition);

	virtual Vector2 GetHalfDimensions() const;

	virtual void SetDesiredDirection(const Vector2 direction) { m_desiredDirection.Set(direction); }

	virtual std::string ToString() const { return "Player Ship"; }

	virtual CollisionType GetCollisionType() const { return (CollisionType::PLAYER | CollisionType::SHIP); }

	virtual void ConfineToScreen(const bool isConfined = true) { m_isConfinedToScreen = isConfined; }

	virtual void ActivateShield() { m_pShield->Activate(); }

	virtual PlayerShipShield* GetShield() { return m_pShield; }

protected:

	virtual void SetResponsiveness(const float responsiveness);

	virtual float GetResponsiveness() const { return m_responsiveness; }

	virtual Vector2 GetDesiredDirection() const { return m_desiredDirection; }

private:

	PlayerShipShield *m_pShield;

	Vector2 m_desiredDirection;
	Vector2 m_velocity;

	float m_responsiveness;

	bool m_isConfinedToScreen;

	Texture *m_pTexture;
};