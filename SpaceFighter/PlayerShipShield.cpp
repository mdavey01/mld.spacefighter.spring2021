#include "PlayerShipShield.h"

void PlayerShipShield::LoadContent(ResourceManager *pResourceManager)
{
	ConfineToScreen(false);
	SetCollisionRadius(45);
	m_pTexture = pResourceManager->Load<Texture>("Textures\\Shield.png");
}

void PlayerShipShield::Activate()
{
	SetHitPoints(3);
	GameObject::Activate();
}

void PlayerShipShield::Update(Vector2 PSPlace)//PlayerShip *pPlayerShip)
{
	SetPosition(PSPlace);
}

void PlayerShipShield::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter());
	}
}

void PlayerShipShield::Hit(float damage)
{
	if (!m_isInvulnurable)
	{
		m_hitPoints -= damage;

		if (m_hitPoints <= 0)
		{
			GameObject::Deactivate();
		}
	}
}
