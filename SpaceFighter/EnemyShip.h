
#pragma once

#include "Ship.h"
#include "PowerUpManager.h"

class EnemyShip : public Ship
{

public:

	EnemyShip();
	virtual ~EnemyShip() { }

	virtual void SetPowerUpManager(PowerUpManager* pPowerUpManager) { m_powerUpManager = pPowerUpManager; }

	virtual void Update(const GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch) = 0;  // EX: Pure Virtual

	virtual void Initialize(const Vector2 position, const double delaySeconds);

	virtual void Fire() { }

	virtual void Hit(const float damage, const CollisionType collisionType);

	virtual std::string ToString() const { return "Enemy Ship"; }

	virtual CollisionType GetCollisionType() const { return CollisionType::ENEMY | CollisionType::SHIP; }

	virtual float GetDamage() { return m_collisionDamage; }

protected:

	virtual double GetDelaySeconds() const { return m_delaySeconds; }


private:

	double m_delaySeconds;

	double m_activationSeconds;

	PowerUpManager* m_powerUpManager;

	float m_collisionDamage = 1.0f;

};
