
#include "PowerUpManager.h"

PowerUpManager::PowerUpManager()
{
	m_attemptCount = 0;
	m_powerUpDrops = nullptr;
}

bool PowerUpManager::TryGetPowerUpDrop(PowerUpDrop** pDrop)
{
	m_attemptCount++;
	if (m_attemptCount % DropFrequency != 0)
	{
		return false;
	}
	PowerUpDrop* drop = GetPowerUpDrop();
	if (drop)
	{
		drop->SetPowerUpType(GetDropType());
		*pDrop = drop;
		return true;
	}
	return false;
}

PowerUpType PowerUpManager::GetDropType()
{
	int temp = rand() % 2 + 1;
	if (temp == 1)
		return PowerUpType::Shield;
	else if (temp == 2)
		return PowerUpType::WeaponSpeed;
}


PowerUpDrop* PowerUpManager::GetPowerUpDrop()
{
	m_powerUpDropIt = m_powerUpDrops->begin();
	for (; m_powerUpDropIt != m_powerUpDrops->end(); m_powerUpDropIt++)
	{
		PowerUpDrop* pPowerupDrop = *m_powerUpDropIt;
		if (!pPowerupDrop->IsActive())
			return pPowerupDrop;
	}
	return nullptr;
}