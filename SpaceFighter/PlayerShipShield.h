#pragma once
#include "GameObject.h"

class PlayerShipShield : public GameObject
{
public:

	PlayerShipShield() {}
	virtual ~PlayerShipShield() { }

	virtual void LoadContent(ResourceManager *pResourceManager);

	virtual void Activate();

	virtual void Update(Vector2 PSPlace);//PlayerShip* pPlayerShip);

	virtual void Draw(SpriteBatch *pSpriteBatch);

	virtual std::string ToString() const { return "Shield"; }

	virtual CollisionType GetCollisionType() const { return (CollisionType::PLAYER| CollisionType::SHIELD); }

	virtual void ConfineToScreen(const bool isConfined = false) { m_isConfinedToScreen = isConfined; }


	virtual bool IsInvulnurable() const { return m_isInvulnurable; }

	virtual void SetInvulnurable(bool isInvulnurable = false) { m_isInvulnurable = isInvulnurable; }

	virtual bool GetIsInvulnerable() { return m_isInvulnurable; }

	virtual float GetHitPoints() { return m_hitPoints; }

	virtual void Hit(const float damage);

	virtual float GetDamage() { return m_collisionDamage; }

protected:

	//virtual void Initialize();

	virtual void SetHitPoints(float hp) { m_hitPoints = hp; }

	//virtual float GetHP(float hp) { return m_hitPoints; }

private:

	Texture *m_pTexture;

	bool m_isInvulnurable;

	bool m_isConfinedToScreen;

	float m_hitPoints = 0;

	float m_collisionDamage = 1.0f;

};

