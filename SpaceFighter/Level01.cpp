
#include "Level01.h"
#include "BioEnemyShip.h"


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ship sprite
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");
	// Set how many enemies for the level
	const int COUNT = 21;
	// Set where they will appear on screen.  (Screen width = 1.0)
	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};
	// Set when they will appear on screen.  (all times += previous value.)
	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3
	};
	// Set delay before enemies appear
	float delay = 3.0;  // original value
	//float delay = 5.0;  // altered value
	Vector2 position;  // Temp value for storing initial enemy ship screen position

	PowerUpManager* pPowerUpManager = CreatePowerUpManager(&m_powerUpDrops);

	// Build the enemy ship objects for eventual display on screen
	for (int i = 0; i < COUNT; i++)
	{
		// Determine actual delay time for each ESO (Enemy Ship Object) 
		delay += delays[i];
		// Determine actual position of ESO on screen per users actual screen width.
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);
		// Create ESO
		BioEnemyShip *pEnemy = new BioEnemyShip();
		// Apply Texture to, record level for and initialize the ESO
		pEnemy->SetPowerUpManager(pPowerUpManager);
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		//Add ESO to the GameObject.
		AddGameObject(pEnemy);
	}
	// Add in default level objects (i.e., player's ship, etc.)
	Level::LoadContent(pResourceManager);
}

