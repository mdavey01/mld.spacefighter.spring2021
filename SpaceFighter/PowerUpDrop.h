
#pragma once

#include "KatanaEngine.h"
#include "GameObject.h"
#include "PlayerShip.h"

enum PowerUpType
{
	None = 0,
	Shield = 1,
	WeaponSpeed = 2
};

class PowerUpDrop : public GameObject
{
public:

	PowerUpDrop();
	virtual ~PowerUpDrop() { }

	static void LoadContent(ResourceManager* pResourceManager);

	/** @brief Updates the PowerUps screen position.
		@param pGameTime Timing values including time since last update. */
	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);

	virtual void Activate(const Vector2& position, bool m_wasDroppedByEnemy = true);

	virtual float GetType() const { return m_type; }

	virtual std::string ToString() const;

	virtual CollisionType GetCollisionType() const;

	virtual bool IsDrawnByLevel() const { return m_drawnByLevel; }

	/// <summary>
	/// Sets the PowerUpType this drop will use.
	/// </summary>
	/// <param name="pPowerUpType"></param>
	virtual void SetPowerUpType(const PowerUpType pPowerUpType) { m_powerUpType = pPowerUpType; }

	virtual void SetManualDraw(const bool drawManually = true) { m_drawnByLevel = !drawManually; }

	/// <summary>
	/// Applies this powerup to the PlayerShip.
	/// </summary>
	/// <param name="pShip"></param>
	virtual void ApplyPowerup(PlayerShip* pShip);

	/// <summary>
	/// Returns this drop's current PowerUpType.
	/// </summary>
	/// <returns></returns>
	PowerUpType get_PowerUpType() { return m_powerUpType; }


protected:

	virtual void SetSpeed(const float speed) { m_speed = speed; }

	virtual void SetType(const float type) { m_type = type; }

	virtual void SetDirection(const Vector2 direction) { m_direction = direction; }

	virtual float GetSpeed() const { return m_speed; }

	virtual Vector2& GetDirection() { return m_direction; }

	virtual bool WasDroppedByEnemy() const { return m_wasDroppedByEnemy; }

	virtual CollisionType GetPowerUpType() const { return CollisionType::POWERUP; }

	virtual std::string GetPowerUpTypeString() const { return "Power Up"; }


private:

	static Texture* s_pTextures[];
	Texture* s_pTexture;

	float m_speed;
	float m_type;  

	Vector2 m_direction;

	bool m_wasDroppedByEnemy;

	bool m_drawnByLevel;

	PowerUpType m_powerUpType;

	float m_activeTime;

};

