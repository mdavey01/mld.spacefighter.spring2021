#pragma once

#include "Level.h"

class Level01 :	public Level
{

public:
	
	Level01() { }

	virtual ~Level01() { }

	/** @brief Sets up level 1 of the game.
		@param pResourceManager - Points to the games Reaource Manager*/
	virtual void LoadContent(ResourceManager *pResourceManager);

	virtual void UnloadContent() { }

};

