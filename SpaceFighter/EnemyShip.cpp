
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0)
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();
		}
	}

	if (IsActive())
	{
		m_activationSeconds += pGameTime->GetTimeElapsed();
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage, const CollisionType source)
{
	Ship::Hit(damage, source);
	if (!Ship::IsActive())
	{
		bool collidedWithShip = (source & CollisionType::SHIP) != CollisionType::NONE;
		bool collidedWithShield = (source & CollisionType::SHIELD) != CollisionType::NONE;
		bool dropPowerup = !(collidedWithShip || collidedWithShield);

		PowerUpDrop* pDrop = nullptr;
		if (dropPowerup && m_powerUpManager->TryGetPowerUpDrop(&pDrop))
		{
			pDrop->Activate(GetPosition(), true);
		}
	}
}