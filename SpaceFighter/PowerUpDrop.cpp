#include "PowerUpDrop.h"

Texture* PowerUpDrop::s_pTextures[] = { nullptr, nullptr, nullptr };

void PowerUpDrop::LoadContent(ResourceManager* pResourceManager)
{
	s_pTextures[PowerUpType::None] = pResourceManager->Load<Texture>("Textures\\Drop_base.png");
	s_pTextures[PowerUpType::Shield] = pResourceManager->Load<Texture>("Textures\\Drop_Shield.png");
	s_pTextures[PowerUpType::WeaponSpeed] = pResourceManager->Load<Texture>("Textures\\Drop_WeaponSpeed.png");
}

PowerUpDrop::PowerUpDrop()
{
	SetSpeed(50);
	SetType(1);  // create via random number generator as more types are added.
	SetDirection(Vector2::UNIT_Y);
	SetCollisionRadius(35);
	m_drawnByLevel = true;
}

void PowerUpDrop::ApplyPowerup(PlayerShip *pShip)
{
	switch (m_powerUpType)
	{
	case PowerUpType::Shield:
	{
		pShip->ActivateShield();
		break;
	}
	case PowerUpType::WeaponSpeed:
	{
		pShip->OffsetWeaponSpeed(-0.05f);
		break;
	}
	default:
	{
		break;
	}
	}

}

void PowerUpDrop::Update(const GameTime* pGameTime)
{
	if (IsActive())  // If the PowerUp is active
	{  // Get the direction and speed the PowerUp has moved during the specified game time (Heisenberg would be appalled!)
		Vector2 translation = m_direction * m_speed * pGameTime->GetTimeElapsed();
		m_activeTime += pGameTime->GetTimeElapsed();
		// Convert into an updated screen position
		TranslatePosition(translation);
		// Get new screen position
		Vector2 position = GetPosition();
		// Get size of asset representing object on screen.
		Vector2 size = s_pTexture->GetSize();

		// Is the PowerUp off the screen?  If yes, mark for deletion.
		if (position.Y < -size.Y) Deactivate();
		else if (position.X < -size.X) Deactivate();
		else if (position.Y > Game::GetScreenHeight() + size.Y) Deactivate();
		else if (position.X > Game::GetScreenWidth() + size.X) Deactivate();
	}
	// Send to array for later drawing on screen
	GameObject::Update(pGameTime);
}

void PowerUpDrop::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(s_pTexture, GetPosition(), Color::White, s_pTexture->GetCenter());
	}
}

void PowerUpDrop::Activate(const Vector2& position, bool wasDroppedByEnemy)
{
	m_wasDroppedByEnemy = wasDroppedByEnemy;
	SetPosition(position);
	s_pTexture = s_pTextures[get_PowerUpType()];
	GameObject::Activate();
}

std::string PowerUpDrop::ToString() const
{
	return ((WasDroppedByEnemy()) ? "Player " : "Enemy ") + GetPowerUpTypeString();
}

CollisionType PowerUpDrop::GetCollisionType() const
{
	CollisionType shipType = WasDroppedByEnemy() ? CollisionType::ENEMY : CollisionType::PLAYER;
	return (shipType | CollisionType::POWERUP);
}