
#include "CollisionType.h"

const CollisionType CollisionType::NONE			= CollisionType(0);       // 0000 0000 = 0
const CollisionType CollisionType::PLAYER		= CollisionType(1 << 0);  // 0000 0001 = 1
const CollisionType CollisionType::ENEMY		= CollisionType(1 << 1);  // 0000 0010 = 2
const CollisionType CollisionType::SHIP			= CollisionType(1 << 2);  // 0000 0100 = 4
const CollisionType CollisionType::PROJECTILE	= CollisionType(1 << 3);  // 0000 1000 = 8
const CollisionType CollisionType::SHIELD		= CollisionType(1 << 4);  // 0001 0000 = 16
const CollisionType CollisionType::POWERUP		= CollisionType(1 << 5);  // 0010 0000 = 32