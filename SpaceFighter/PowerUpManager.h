#pragma once
#include "GameObject.h"
#include "PowerUpDrop.h"

class PowerUpManager : public GameObject
{
public:
    PowerUpManager();
    virtual ~PowerUpManager() 
    {
        delete m_powerUpDrops;
    }

    /// <summary>
    /// Sets the pool of PowerUpDrops to use.
    /// </summary>
    /// <param name="pDropPool"></param>
    virtual void SetPowerUpDropPool(std::vector<PowerUpDrop*>* pDropPool) { m_powerUpDrops = pDropPool; }

    virtual void Update(const GameTime* pGameTime) { }

    virtual void Draw(SpriteBatch* pSpriteBatch) { };

    /// <summary>
    /// Returns true if a PowerUpDrop* is set in pDrop.
    /// </summary>
    /// <param name="pDrop"></param>
    /// <returns></returns>
    /// <remarks>
    /// This function controls whether an enemy will drop a PowerUp,
    /// right now it's based on how many times this method has been called.
    /// </remarks>
    virtual bool TryGetPowerUpDrop(PowerUpDrop** pDrop);

    virtual std::string ToString() const { return "PowerUpManager"; }

    virtual CollisionType GetCollisionType() const { return CollisionType::NONE; }

protected:
    /// <summary>
    /// Returns a PowerUpDrop from the pool, if one is available.
    /// </summary>
    /// <returns></returns>
    virtual PowerUpDrop* GetPowerUpDrop();
    /// <summary>
    /// Returns the next PowerUpType. The logic for which powerup will drop
    /// is in here.
    /// </summary>
    /// <returns></returns>
    virtual PowerUpType GetDropType();

private:
    /// <summary>
    /// A PowerUp will drop every x calls to TryGetPowerUpDrop.
    /// </summary>
    const int DropFrequency = 3;
    int m_attemptCount;
    std::vector<PowerUpDrop*>* m_powerUpDrops;
    std::vector<PowerUpDrop*>::iterator m_powerUpDropIt;
};

